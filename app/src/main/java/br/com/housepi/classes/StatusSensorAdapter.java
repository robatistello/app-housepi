package br.com.housepi.classes;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.housepi.R;

public class StatusSensorAdapter extends BaseAdapter {

	private List<SensorAlarme> sensores;

	private ViewBase view;
	private LayoutInflater layout;

	static class ViewBase {
		private TextView txNomeSensor;
		private ImageView imagem;
	}

	public StatusSensorAdapter(Context context, List<SensorAlarme> sensores) {
		layout = LayoutInflater.from(context);
		this.sensores = sensores;
	}

	@Override
	public int getCount() {
		return sensores.size();
	}

	@Override
	public Object getItem(int position) {
		return sensores.get(position);
	}

	@Override
	public long getItemId(int position) {
		return Long.valueOf(position);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = layout.inflate(R.layout.lista_sensor_alarme, null);
			view = new ViewBase();

			view.txNomeSensor = (TextView) convertView.findViewById(R.id.txNomeSensor);
			view.imagem = (ImageView) convertView.findViewById(R.id.imgStatusSensor);

			convertView.setTag(view);
		} else {
			view = (ViewBase) convertView.getTag();

		}

		SensorAlarme s = sensores.get(position);

		view.txNomeSensor.setText(s.getNome());
		if (s.getStatus() == 0) {
			view.imagem.setImageResource(R.drawable.ic_violado);
		} else {
			view.imagem.setImageResource(R.drawable.ic_normal);
		}

		return convertView;
	}

}
