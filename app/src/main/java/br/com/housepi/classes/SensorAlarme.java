package br.com.housepi.classes;

public class SensorAlarme {
    private String nome;

    public SensorAlarme(String nome, Integer status) {
        this.nome = nome;
        this.status = status;
    }

    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
