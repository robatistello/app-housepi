package br.com.housepi.activity;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import br.com.housepi.R;
import br.com.housepi.classes.Conexao;
import br.com.housepi.classes.Funcoes;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

public class TemperaturaHumidade extends Fragment implements View.OnClickListener {
	private String temperatura;
	private String humidade;
	private TextView lblTemperatura;
	private TextView lblHumidade;
	private LinearLayout llTemperatura;
	private LinearLayout llUmidade;
	private LineChart lineChart;
	private ProgressDialog dialog;
	private ArrayList<Historico> historico = new ArrayList<Historico>();
	private ArrayList<Entry> entriesTemperatura = new ArrayList<Entry>();
	private ArrayList<Entry> entriesUmidade = new ArrayList<Entry>();
	private ArrayList<String> labels = new ArrayList<String>();
	private static String SEM_DADOS = "Sem dados para exibir no gráfico";

	public static Fragment newInstance(Context context) {
		return new TemperaturaHumidade();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.temperatura_humidade, container, false);

        llTemperatura = (LinearLayout) rootView.findViewById(R.id.llTemperatura);
        llUmidade     = (LinearLayout) rootView.findViewById(R.id.llUmidade);

        llTemperatura.setOnClickListener(this);
        llUmidade.setOnClickListener(this);

		lblTemperatura = (TextView) rootView.findViewById(R.id.lblTemperatura);
		lblHumidade    = (TextView) rootView.findViewById(R.id.lblHumidade);

		lineChart = (LineChart) rootView.findViewById(R.id.chart);

		lineChart.setNoDataText(SEM_DADOS);
		lineChart.animateY(2000);

		return rootView;
	}

	private void desenharGrafico(){
		entriesTemperatura.clear();
        entriesUmidade.clear();
		labels.clear();

		int a = 0;

		for (Historico registro : historico) {
			entriesTemperatura.add(new Entry(registro.temperatura, a));
            entriesUmidade.add(new Entry(registro.umidade, a));
			labels.add(registro.hora);

			a++;
		}

        String[] horas = new String[labels.size()];
        horas = labels.toArray(horas);

		ArrayList<ILineDataSet> lines = new ArrayList<ILineDataSet> ();

		LineDataSet lDataSet1 = new LineDataSet(entriesTemperatura, "Temperatura");
        lDataSet1.setDrawFilled(true);
		lDataSet1.setValueTextSize(11);
		lines.add(lDataSet1);

        LineDataSet lDataSet2 = new LineDataSet(entriesUmidade, "Umidade");
        lDataSet2.setDrawFilled(true);
		lDataSet2.setColor(Color.GRAY);
		lDataSet2.setCircleColor(Color.GRAY);
		lDataSet2.setFillColor(Color.LTGRAY);
		lDataSet2.setValueTextSize(11);
		lines.add(lDataSet2);

        lineChart.setData(new LineData(horas, lines));
		lineChart.setTouchEnabled(true);
		lineChart.setDescription("");
		lineChart.setNoDataText(SEM_DADOS);
		lineChart.animateY(0);
		lineChart.moveViewToX(entriesTemperatura.size());

		XAxis xAxis = lineChart.getXAxis();
		xAxis.setGridColor(Color.LTGRAY);
		xAxis.setAxisLineColor(Color.LTGRAY);

		YAxis rightAxis = lineChart.getAxisRight();
		rightAxis.setEnabled(false);

		YAxis leftAxis = lineChart.getAxisLeft();
		leftAxis.setXOffset(12);
		leftAxis.setGridColor(Color.LTGRAY);
	}

	public void getTemperaturaHumidade() {
		Document doc = new Document();
		Element root = new Element("Temperatura");
		doc.setRootElement(root);

		Conexao.getConexaoAtual().enviarMensagem(new XMLOutputter().outputString(doc));

		try {
			String mensagem;

			mensagem = Conexao.getConexaoAtual().receberRetorno();

			if (!mensagem.equals("Erro")){

				SAXBuilder builder = new SAXBuilder();
				Reader in = new StringReader(mensagem);

				try {
					doc = builder.build(in);
				} catch (JDOMException e) {
					e.printStackTrace();
				}

				Element retorno = (Element) doc.getRootElement();

				temperatura = retorno.getChild("Dados").getAttribute("Temperatura").getValue() + " ºC";
				humidade = retorno.getChild("Dados").getAttribute("Humidade").getValue() + " %";

				historico.clear();

				try {
					@SuppressWarnings("rawtypes")
					List elements = retorno.getChild("Historico").getChildren();
					@SuppressWarnings("rawtypes")
					Iterator j = elements.iterator();

					while (j.hasNext()) {
						Element element = (Element) j.next();

						Historico registro = new Historico();

						registro.hora = element.getAttribute("Hora").getValue();
						registro.temperatura = Float.parseFloat(element.getAttribute("Temperatura").getValue());
						registro.umidade = Float.parseFloat(element.getAttribute("Humidade").getValue());

						historico.add(registro);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				Message msg = new Message();
				msg.arg1 = 1;
				handler.sendMessage(msg);

				dialog.dismiss();
			} else {
				Message msg = new Message();
				msg.arg1 = 0;
				handler.sendMessage(msg);

				dialog.dismiss();
			}
		} catch (IOException e) {
			dialog.dismiss();
			e.printStackTrace();
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			try {
				synchronized (msg) {
					if (msg.arg1 == 0) {
						Funcoes.msgToastErroComando(getActivity());
					} else {
						if (!temperatura.trim().equals("")) {
							lblTemperatura.setText(temperatura);
						}

						if (!humidade.trim().equals("")) {
							lblHumidade.setText(humidade);
						}

						if (!historico.isEmpty()) {
							desenharGrafico();
						}
					}
				}
			} catch (Exception e) {
				dialog.dismiss();

				startThreadGetDados();
			}
		}
	};

	private void startThreadGetDados() {
		dialog = ProgressDialog.show(this.getActivity(), "", "Aguarde...");

		new Thread() {
			public void run() {
				try{
					getTemperaturaHumidade();
				} catch (Exception e) {
					Log.e("tag", e.getMessage());
				}
			}
		}.start();
	}

	@Override
	public void onResume() {
		super.onResume();

		if ((dialog == null) || (!dialog.isShowing())) {
			startThreadGetDados();
		}
	}

	@Override
	public void onClick(View view) {
		if ((view == llTemperatura) || (view == llUmidade)) {
			if ((dialog == null) || (!dialog.isShowing())) {
				startThreadGetDados();
			}
		}
	}

	public class Historico {
		public String hora;
		public float temperatura;
		public float umidade;
	}
}
