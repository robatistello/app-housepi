package br.com.housepi.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import br.com.housepi.R;
import br.com.housepi.classes.Conexao;
import br.com.housepi.classes.Funcoes;

public class VisualizacaoLog extends Fragment {

    private ListView listView;
    private ArrayAdapter<String> adapter;
    private List<String> log = new LinkedList<String>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.visualizacao_log, container, false);

        listView = (ListView) rootView.findViewById(R.id.lvLog);

        adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.linha_list_view_log, R.id.listTextoLog, log);

        listView.setAdapter(adapter);

        getLog();

        return rootView;
    }

    private void getLog() {
        try {
            Document doc = new Document();
            Element root = new Element("EnviarLog");
            doc.setRootElement(root);

            Conexao.getConexaoAtual().enviarMensagem(new XMLOutputter().outputString(doc));

            String mensagem = "";
            log.clear();

            mensagem = Conexao.getConexaoAtual().receberRetorno();

            SAXBuilder builder = new SAXBuilder();
            Reader in = new StringReader(mensagem);

            try {
                doc = builder.build(in);
            } catch (JDOMException e) {
                e.printStackTrace();
            }

            Element retorno = (Element) doc.getRootElement();

            if (!retorno.getName().equals("EnviarLog")) {
                Funcoes.msgToastErroComando(this.getActivity());
                return;
            }

            @SuppressWarnings("rawtypes")
            List elements = retorno.getChildren();
            @SuppressWarnings("rawtypes")
            Iterator j = elements.iterator();

            while (j.hasNext()) {
                Element element = (Element) j.next();

                String texto = element.getAttribute("Texto").getValue();

                log.add(texto);
            }

            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
