package br.com.housepi.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.text.Layout;
import android.text.method.ScrollingMovementMethod;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jcraft.jsch.*;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import br.com.housepi.R;
import br.com.housepi.classes.Banco;
import br.com.housepi.classes.Funcoes;

public class InstalacaoServidor extends ActionBarActivity {
    private String host;
    private Integer porta;
    private String usuario;
    private String senha;
    private Context context = this;
    private TextView tvLog;
    private String erro = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instalacao_servidor);

        tvLog = (TextView) findViewById(R.id.tvLog);
        tvLog.setMovementMethod(new ScrollingMovementMethod());

        solicitarDadosConexao();
    }

    private void solicitarDadosConexao() {
        Banco banco = new Banco(context);
        SQLiteDatabase acessaBanco = banco.getWritableDatabase();

        String descricao = Funcoes.carregarDadosComponente("ConexaoSelecionada", "", this);

        Cursor c = acessaBanco.query("Conexao", new String[]{"Host"}, "Descricao=?", new String[]{descricao}, null, null, null, null);

        if (c.getCount() > 0) {
            c.moveToFirst();

            host = c.getString(0);
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("SSH");
        alert.setMessage("Informe os dados do seu rpi para estabelecer a conexão via SSH");

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText edtHost = new EditText(this);
        final EditText edtPorta = new EditText(this);
        final EditText edtUsuario = new EditText(this);
        final EditText edtSenha = new EditText(this);

        edtHost.setHint("Host Ex: 192.168.1.5");
        edtPorta.setHint("Porta SSH Ex: 22");
        edtUsuario.setHint("Usuário Linux Ex: pi");
        edtSenha.setHint("Senha Linux Ex: raspberry");

        edtHost.setText(host);
        edtPorta.setText(R.string.porta_ssh);
        edtUsuario.setText(R.string.usuario_rpi);

        edtPorta.setInputType(InputType.TYPE_CLASS_NUMBER);

        edtSenha.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edtSenha.requestFocus();

        layout.addView(edtHost);
        layout.addView(edtPorta);
        layout.addView(edtUsuario);
        layout.addView(edtSenha);

        ScrollView scroll = new ScrollView(context);
        scroll.setBackgroundColor(android.R.color.transparent);
        scroll.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT));
        scroll.addView(layout);

        alert.setView(scroll);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                host = edtHost.getText().toString();
                porta = Integer.parseInt(edtPorta.getText().toString());
                usuario = edtUsuario.getText().toString();
                senha = edtSenha.getText().toString();

                EfetuarInstalacao();
            }
        });

        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ((Activity) context).finish();
            }
        });

        alert.show();
    }

    private void EfetuarInstalacao() {
        new AsyncTask<String, String, Long>() {
            ProgressDialog progressDialog;

            @Override
            protected Long doInBackground(String... urls) {
                try {
                    JSch jsch = new JSch();
                    Session session = jsch.getSession(usuario, host, porta);
                    session.setPassword(senha);

                    Properties prop = new Properties();
                    prop.put("StrictHostKeyChecking", "no");
                    session.setConfig(prop);

                    session.connect();

                    Channel channel = session.openChannel("shell");
                    channel.connect();

                    DataInputStream dataIn = new DataInputStream(channel.getInputStream());
                    DataOutputStream dataOut = new DataOutputStream(channel.getOutputStream());

                    dataOut.writeBytes("curl https://bitbucket.org/robatistello/srv-housepi/raw/36e1f2d50296c4c9d690281482b8d56e7b111731/setup.sh | sudo sh\n");
                    dataOut.flush();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(dataIn));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        publishProgress(line);

                        if (line.equals("Instalacao concluida.")) {
                            notificarInstalacaoConcluida();

                            break;
                        }
                    }

                    dataIn.close();
                    dataOut.close();
                    channel.disconnect();
                    session.disconnect();
                } catch (Exception e) {
                    erro = e.getMessage();
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(String... update) {
                appendTextAndScroll(update[0]);
            }

            @Override
            protected void onPreExecute() {
                progressDialog = new ProgressDialog(InstalacaoServidor.this);
                progressDialog.setMessage("Instalando... Isso pode levar alguns minutos. Você será avisado assim que a instalação terminar!");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }

            @Override
            protected void onPostExecute(Long aLong) {
                super.onPostExecute(aLong);

                progressDialog.cancel();

                if (!erro.isEmpty()) {
                    erro = erro + "\n\nVerifique os dados da conexão e tente novamente!";

                    appendTextAndScroll(erro);

                    Funcoes.msgDialogoInformacao("Erro na instalação", erro, context);
                }
            }
        }.execute();
    }

    private void notificarInstalacaoConcluida() {
        Intent it = new Intent(context, Login.class);
        PendingIntent pi = PendingIntent.getActivity(context, 0, it, 0);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification nt = new Notification(R.drawable.ic_launcher, "Instalação concluída!", System.currentTimeMillis());
        nt.setLatestEventInfo(context, "House Pi", "Instalação concluída! Reiniciando RPI...", pi);

        nt.vibrate = new long[]{150, 300, 150, 300};
        nt.flags |= Notification.FLAG_AUTO_CANCEL;
        nm.notify(R.string.app_name, nt);
    }

    private void appendTextAndScroll(String text) {
        if (tvLog != null) {
            tvLog.append(text + "\n");
            final Layout layout = tvLog.getLayout();
            if (layout != null) {
                int scrollDelta = layout.getLineBottom(tvLog.getLineCount() - 1) - tvLog.getScrollY() - tvLog.getHeight();
                if (scrollDelta > 0)
                    tvLog.scrollBy(0, scrollDelta);
            }
        }
    }
}
