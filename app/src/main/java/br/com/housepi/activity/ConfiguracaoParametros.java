package br.com.housepi.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import java.io.Reader;
import java.io.StringReader;

import br.com.housepi.R;
import br.com.housepi.classes.Conexao;
import br.com.housepi.classes.Funcoes;

public class ConfiguracaoParametros extends Fragment implements View.OnClickListener {
    private EditText edtSirene;
    private EditText edtDHT;
    private EditText edtMusica;
    private EditText edtVideo;
    private Button btnSalvar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.configuracao_parametros, container, false);

        edtSirene = (EditText) rootView.findViewById(R.id.edtParametroSirene);
        edtDHT = (EditText) rootView.findViewById(R.id.edtParametroDHT);
        edtMusica = (EditText) rootView.findViewById(R.id.edtParametroMusica);
        edtVideo = (EditText) rootView.findViewById(R.id.edtParametroVideo);

        btnSalvar = (Button) rootView.findViewById(R.id.btnSalvarConfParametros);
        btnSalvar.setOnClickListener(this);

        carregarDados();

        return rootView;
    }

    private void carregarDados() {
        try {
            Document doc = new Document();
            Element root = new Element("EnviarParametroConfiguracao");
            doc.setRootElement(root);

            Conexao.getConexaoAtual().enviarMensagem(new XMLOutputter().outputString(doc));

            String mensagem = "";

            mensagem = Conexao.getConexaoAtual().receberRetorno();

            SAXBuilder builder = new SAXBuilder();
            Reader in = new StringReader(mensagem);

            try {
                doc = builder.build(in);
            } catch (JDOMException e) {
                e.printStackTrace();
            }

            Element retorno = (Element) doc.getRootElement();

            if (!retorno.getName().equals("EnviarParametroConfiguracao")) {
                Funcoes.msgToastErroComando(this.getActivity());
                return;
            }

            edtSirene.setText(retorno.getChild("Dados").getAttribute("GPIOSirene").getValue());
            edtDHT.setText(retorno.getChild("Dados").getAttribute("GPIODHT").getValue());
            edtMusica.setText(retorno.getChild("Dados").getAttribute("DiretorioMusica").getValue());
            edtVideo.setText(retorno.getChild("Dados").getAttribute("DiretorioVideo").getValue());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void salvarDados() {
        String mensagem = "";

        Document doc = new Document();
        Element root = new Element("AlterarParametroConfiguracao");

        Element sirene = new Element("GPIOSirene");
        sirene.setText(edtSirene.getText().toString());
        root.addContent(sirene);

        Element dht = new Element("GPIODHT");
        dht.setText(edtDHT.getText().toString());
        root.addContent(dht);

        int tamanho;

        tamanho = edtMusica.getText().toString().length();

        if (!edtMusica.getText().toString().substring(tamanho - 1, tamanho).equals("/")) {
            edtMusica.setText(edtMusica.getText().toString() + "/");
        }

        Element musica = new Element("DiretorioMusica");
        musica.setText(edtMusica.getText().toString());
        root.addContent(musica);

        tamanho = edtVideo.getText().toString().length();

        if (!edtVideo.getText().toString().substring(tamanho - 1, tamanho).equals("/")) {
            edtVideo.setText(edtVideo.getText().toString() + "/");
        }

        Element video = new Element("DiretorioVideo");
        video.setText(edtVideo.getText().toString());
        root.addContent(video);

        doc.setRootElement(root);

        mensagem = new XMLOutputter().outputString(doc);
        Conexao.getConexaoAtual().enviarMensagem(mensagem);

        mensagem = Conexao.getConexaoAtual().receberRetorno();

        if (mensagem.equals("Ok")) {
            Funcoes.msgToastDadosGravados(this.getActivity());
        } else {
            Funcoes.msgToastErroGravar(this.getActivity());
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnSalvar) {
            if (edtSirene.getText().toString().equals("") || edtDHT.getText().toString().equals("") ||
                    edtMusica.getText().toString().equals("") || edtVideo.getText().toString().equals("")) {
                Toast.makeText(this.getActivity(), "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
            } else {
                salvarDados();
            }
        }
    }
}
